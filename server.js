const protobuf = require('protobufjs');
const bodyParser = require('body-parser');
const express = require('express');

const app = express();

const BytesValue = protobuf.Root.fromJSON(protobuf.common.get('google/protobuf/wrappers.proto')).lookup('BytesValue');
const ProcessEventProto = protobuf.loadSync('turiya-data.proto').lookupType('com.protium.apps.turiya.data.model.ProcessEventProto');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.raw({type: 'application/x-protobuf'}));

app.get('/', (req, res) => res.send('Hello world'));

app.post('/echo', async (req, res) => {
    console.log(req.url);
    console.log(req.body);
    res.send(req.body);
});

app.post('/echo/logs', async (req, res) => {
    console.log(req.url);
    console.log(req.headers);

    const reader = protobuf.Reader.create(req.body);

    while (reader.pos < reader.len) {
        console.log(String(BytesValue.decodeDelimited(reader).value));
    }

    res.send({url: req.url});
});

app.post('/echo/events', async (req, res) => {
    console.log(req.url);
    console.log(req.headers);

    const reader = protobuf.Reader.create(req.body);

    while (reader.pos < reader.len) {
        const event = ProcessEventProto.decodeDelimited(reader);
        console.log(event.application.name + ' - ' + event.businessKey.root + ' - ' + event.stage.name + ' - ' + event.stage.action);
    }

    res.send({url: req.url});
});

app.listen(8080, '0.0.0.0', () => console.log("server is listening"))

console.log('server started');
