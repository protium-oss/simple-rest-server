FROM node:21-bookworm

RUN mkdir -p /app

WORKDIR /app

COPY server.js /app/
COPY package.json /app/
COPY turiya-data.proto /app/

RUN yarn install

EXPOSE 8080

CMD [ "yarn", "start" ]
